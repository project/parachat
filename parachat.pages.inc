<?php

/**
 * Chat Page
 */
function parachat_page() {
  global $user;

  // get parachat settings
  $data = variable_get('parachat_settings', false);
  $data = $data?unserialize($data):array();
  // display error and no chat room if they have not configured parachat yet
  if (!data) {
    drupal_set_message(t('Parachat has not been configured yet'), 'error');
    return '';
  }
  
  // set code base url based on server id
  $codebase = 'http://server'. $data['server_id'] .'.parachat.com/pchat/applet';
  
  // create pass hash or send false if external auth isn't setup
  if ($data['auth']) {
    $result = db_fetch_object(db_query("SELECT name, pass FROM {users} WHERE uid=%d", $user->uid));
    $pass = $user->uid .'-'. md5($result->name . $result->pass);
  }
  else {
    $pass = false;
  }
  
  // option for admin to set custom name
  if (user_access('administer chat')) {
    if ($_GET['user']) {
      $username = $_GET['user'];
      $pass = false;
    }
  }
  
  // set username
  $username = $user->name;
  
  // set theme
  $output = theme('parachat', $codebase, $width, $data['height'], $data['site_id'], $data['room'], $username, $pass);

  return $output;
  
}

/**
 * Page that parachat calls to identify the user
 */
function parachat_auth_page() {
  
  header('content-type: text/plain');
  
  // if auth is turned off, just return result succes in case they left external db on in the parachat admin
  $data = variable_get('parachat_settings', false);
  if ($data) {
    $data = unserialize($data);
    if (!$data['auth']) {
      echo 'Result=Success';
      exit;
    }
  }
  
  $input = $_GET['pass'];
  $uid = substr($input, 0, 1);
  
  $result = db_fetch_object(db_query("SELECT name, pass FROM {users} WHERE uid=%d", $uid));
  $hash = $uid .'-'. md5($result->name . $result->pass);

  

  if ($hash == $input) {
    echo 'Result=Success';
    exit;
  }
  
  echo 'Result=Error';
  exit;
}