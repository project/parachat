<?php

/**
 * @file Parachat admin functions
 */

/**
 * Parachat Settings
 */
function parachat_admin_settings() {
  
  $data = variable_get('parachat_settings', false);
  $data = $data?unserialize($data):array();
  
  $form['admin_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Service administration page'),
    '#description' => t('Sent with Parachat welcome email. i.e. http://server99.parachat.com:8888/sp/login.jsp'),
    '#default_value' => $data['admin_page'],
    '#required' => TRUE
  );
  
  $form['site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#description' => t('Sent with Parachat welcome email. i.e. 1234_demo'),
    '#default_value' => $data['site_id'],
    '#required' => TRUE
    );
    
  $form['room'] = array(
    '#type' => 'textfield',
    '#title' => 'Default Room',
    '#description' => t('Name of the default room users will join'),
    '#default_value' => $data['room'],
    '#required' => TRUE
    );
    
  $options = array(0=>t('No'), 1=>t('Yes'));
  
  $form['auth'] = array(
    '#type' => 'radios',
    '#title' => 'External DB Authentication',
    '#description' => t('This is for added security, but requires more setup in the Parachat UI. External db must be enabled and you need to set the URL to: http://example.com/chat/auth'),
    '#options' => $options,
    '#default_value' => $data['auth']?1:0,
    '#required' => TRUE
    );

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Width of the Parachat applet in pixels'),
    '#default_value' => $data['width']?$data['width']:750,
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE
    );
    
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Height of the Parachat applet in pixels'),
    '#default_value' => $data['height']?$data['height']:500,
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE
    );
    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
    );
    
    return $form;
}

/**
 * Implementation of hook_submit
 */
function parachat_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  // set server id from admin page
  $matches = array();
  preg_match('/server([0-9]+)\.para/', $values['admin_page'], $matches);
  $values['server_id'] = $matches[1];
  
  $data = serialize($values);
  variable_set('parachat_settings', $data);
  drupal_set_message(t('Parachat settings successfully saved.'));  
}